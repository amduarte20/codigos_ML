Para entrar em contato se desejado:

E-mail: amduarte20@gmail.com

Telefone: (021) 9 9989-5304

Neste repositório são encontrados alguns arquivos elaborados com códigos em linguagem de programação Python
voltada para aplicação em Machine Learning.

É aconselhável a aquisição de:

1 - Pacote Anaconda (versão 2020.11)

2 - Programa Python (versões 3.5 a 3.8)

3 - Pacote Keras (baixei a versão 2.4.3)

4 - Pacote Tensorflow (versão 2.2)

5 - Pacote Pandas (baixei a versão 1.1.3)

6 - Pacote Numpy (baixei a versão 1.19.2)
